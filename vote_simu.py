from tkinter import *
from threading import Thread
from time import sleep
from random import randrange, random
from sys import exit

NOTK = True

N = 50
PX = 10
K = 4
P = 0.5
distrib = [1/2, 1/2]
#distrib = [2/5, 2/5, 1/5]
#distrib = [1/3, 1/3, 1/3]

################################
### End constant definitions ###
################################

def from_distr(distrib):
    r = random()
    p = 0
    for i in range(len(distrib)):
        p += distrib[i]
        if r < p:
            return i
    return(len(distrib))

COLORS = len(distrib)

class Simu(object if NOTK else Thread):
    def __init__(self, n, transition, data):
        if not NOTK:
            Thread.__init__(self)
        self.data = data
        self.iternb = 0
        #colortag
        self.count = [0] * COLORS
        for i in range(n * n):
            c = data[i] // (len(transition) // COLORS)
            self.count[c] += 1
        self.connect = [[] for _ in range(n * n)]
        for i in range(n):
            for j in range(n):
                for di, dj in [(0, 1), (1, 0), (0, -1), (-1, 0)]:
                    ni, nj = i + di, j + dj
                    if 0 <= ni < n and 0 <= nj < n:
                        self.connect[n * i + j].append(n * ni + nj)
                if random() < P:
                    self.connect[n*i+j].append(randrange(n*n))
        self.transition = transition

    def run(self):
        i = 0
        while True:
            self.iternb += 1
            u = randrange(len(self.data))
            v = self.connect[u][randrange(len(self.connect[u]))]
            if u == v: continue
            uu, vv = self.data[u], self.data[v]
            self.data[u], self.data[v] = self.transition[uu][vv], self.transition[vv][uu]
            size = len(self.transition) // COLORS
            # colortag
            uu, vv = uu // size, vv // size
            uuu, vvv = self.data[u] // size, self.data[v] // size
            self.count[uu] -= 1
            self.count[vv] -= 1
            self.count[uuu] += 1
            if self.count[uuu] == len(self.data):
                print(self.iternb)
                break
            self.count[vvv] += 1
            if self.count[vvv] == len(self.data):
                print(self.iternb)
                break
            if not NOTK:
                sleep(1e-9)

def color(u, v, w):
    a = int(u * 255)
    b = int(v * 255)
    c = int(w * 255)
    return "#%02x%02x%02x" % (a, b, c)

def build2(k):
    cmap = []
    for i in range(k):
        x = i / (3 * (k - 1))
        cmap.append(color(1,1-x,x))
    for i in range(k):
        x = 2./3 + (k-1-i) / (3. * (k - 1))
        cmap.append(color(1,1-x,x))
    transition = [[None] * (2 * k) for _ in range(2 * k)]
    for i in range(k):
        for j in range(i + 1):
            u, v = (i + j + 1) // 2, (i + j) // 2
            transition[i][j] = v
            transition[j][i] = u
            transition[(k-1-i)+k][(k-1-j)+k] = (k-1-v)+k
            transition[(k-1-j)+k][(k-1-i)+k] = (k-1-u)+k
    for i in range(k):
        for j in range(k):
            z = i + j + k - 1
            if z < 2 * (k - 1):
                transition[i][(k-1-j)+k] = (z+1) // 2
                transition[(k-1-j)+k][i] = z // 2
            elif z == 2 * (k - 1):
                transition[i][(k-1-j)+k] = 2 * k - 1
                transition[(k-1-j)+k][i] = k - 1
            else:
                transition[i][(k-1-j)+k] = 3*k-1-((z+1)//2 + 1)
                transition[(k-1-j)+k][i] = 3*k-1-(z//2 + 1)
    return transition, cmap

def build3(k):
    sst = []
    def norm2(i, j):
        return i * i + j * j - i * j
    def dist(u, v):
        return norm2(u[0] - v[0], u[1] - v[1]) ** 0.5
    for i in range(2 * k, -1, -1):
        for j in range(2 * k, -1, -1):
            if 2 * i - j <= 2 * k and 2 * j - i <= 2 * k:
                sst.append((i, j, norm2(i, j)))
    dd = {}
    pts = []
    for c, bs in enumerate([((0, 1), (1, 0)), ((-1, -1), (0, 1)), ((1, 0), (-1, -1))]):
        for x, y, p in sst:
            u = x * bs[0][0] + y * bs[1][0]
            v = x * bs[0][1] + y * bs[1][1]
            pts.append((u, v, c, x, y, p))
            dd.setdefault((u, v), []).append(len(pts) - 1)
    transition = [[None] * len(pts) for _ in range(len(pts))]
    for i in range(len(pts)):
        for j in range(len(pts)):
            uu, vv = pts[i][0] + pts[j][0], pts[i][1] + pts[j][1]
            if uu % 2 == vv % 2 == 0:
                p1 = p2 = uu // 2, vv // 2
            elif uu % 2 == 0 and vv % 2 == 1:
                p1 = uu // 2, vv // 2
                p2 = uu // 2, vv // 2 + 1
                if p2 not in dd:
                    p1 = uu // 2 - 1, vv // 2
                    p2 = uu // 2 + 1, vv // 2 + 1
            elif uu % 2 == 1 and vv % 2 == 0:
                p1 = uu // 2, vv // 2
                p2 = uu // 2 + 1, vv // 2
                if p2 not in dd:
                    p1 = uu // 2, vv // 2 - 1
                    p2 = uu // 2 + 1, vv // 2 + 1
            else:
                p1 = uu // 2, vv // 2
                p2 = uu // 2 + 1, vv // 2 + 1
                if p1 not in dd:
                    p1 = uu // 2 + 1, vv // 2
                    p2 = uu // 2, vv // 2 + 1
            if dist(pts[i], p1) + dist(pts[j], p2) < dist(pts[j], p1) + dist(pts[i], p2):
                p1, p2 = p2, p1
            np1 = dd[p1]
            c1 = { pts[x][2] for x in np1 }
            np2 = dd[p2]
            c2 = { pts[x][2] for x in np2 }
            np1 = [x for x in np1 if pts[x][2] in c2]
            np2 = [x for x in np2 if pts[x][2] in c1]
            for r1 in np1:
                if pts[r1][2] == pts[j][2]:
                    n1 = r1
                    break
            else:
                n1 = np1[0]
                if len(np1) > 1:
                    print(i, j, pts[i], pts[j], p1, p2, dd[p1], dd[p2])
            for r2 in np2:
                if pts[r2][2] == pts[i][2]:
                    n2 = r2
                    break
            else:
                n2 = np2[0]
                if len(np2) > 1:
                    print(i, j, pts[i], pts[j], p1, p2, dd[p1], dd[p2])
            transition[i][j] = n1
            transition[j][i] = n2
    colormap = [None] * len(pts)
    for i in range(len(pts)):
        a, b, c = 2 * k + pts[i][0] + pts[i][1], 2 * k - 2 * pts[i][0] + pts[i][1], 2 * k + pts[i][0] - 2 * pts[i][1]
        Z = 3 * k
        if pts[i][2] == 0:
            a += Z
        elif pts[i][2] == 1:
            b += Z
        else:
            c += Z
        colormap[i] = color(a / (a + b + c), b / (a + b + c), c / (a + b + c))
    return transition, colormap

def permute(tr, p):
    ip = [0] * len(p)
    for i in range(len(p)):
        ip[p[i]] = i
    return [[ip[tr[p[i]][p[j]]] for j in range(len(p))] for i in range(len(p))]

'''
colormap2 = ["white", "black", "#aaaaaa", "#555555"]

transition2 = [[0, 3, 2, 2], [2, 1, 3, 3], [0, 1, 2, 3], [0, 1, 2, 3]]

transition2_bis = [[0, 1, 1, 1, 2, 3], [0, 1, 2, 3, 2, 3],
                   [1, 1, 2, 3, 4, 4], [1, 1, 2, 3, 4, 4],
                   [1, 3, 3, 3, 4, 5], [2, 4, 4, 4, 4, 5]]

colormap2_bis = ["white", "#dddddd", "#aaaaaa", "#555555", "#222222", "black"]

transition3 =[[0, 1, 2, 3, 3, 7, 4, 1, 1, 3, 11, 2, 4, 2, 3],
               [0, 1, 3, 3, 4, 5, 9, 7, 8, 9, 13, 3, 9, 14, 9],
               [0, 4, 2, 3, 4, 8, 13, 4, 9, 14, 10, 11, 13, 13, 14],
               [0, 1, 2, 3, 4, 8, 9, 1, 9, 4, 13, 2, 14, 11, 4],
               [3, 1, 2, 3, 4, 8, 6, 7, 8, 9, 13, 11, 12, 13, 14],
               [1, 7, 9, 7, 8, 5, 6, 7, 8, 8, 12, 9, 6, 6, 8],
               [3, 8, 14, 4, 9, 5, 6, 8, 8, 9, 10, 14, 12, 13, 14],
               [0, 1, 3, 3, 4, 5, 9, 7, 8, 9, 13, 3, 9, 14, 4],
               [3, 7, 4, 1, 9, 5, 6, 7, 8, 9, 13, 14, 6, 14, 9],
               [3, 1, 2, 3, 4, 8, 6, 7, 8, 9, 13, 11, 12, 13, 14],
               [2, 14, 11, 11, 13, 6, 12, 14, 12, 13, 10, 11, 12, 13, 13],
               [0, 4, 2, 3, 4, 8, 13, 4, 9, 4, 10, 11, 13, 13, 14],
               [3, 8, 14, 4, 9, 5, 6, 8, 8, 9, 10, 14, 12, 13, 14],
               [3, 4, 11, 4, 14, 8, 12, 9, 6, 14, 10, 11, 12, 13, 14],
               [3, 1, 2, 3, 4, 8, 6, 7, 8, 9, 13, 11, 12, 13, 14]]

colormap3 = ["#ff0000", "#aa5500", "#aa0055", "#dd2222", "#aa5555",
             "#00ff00", "#00aa55", "#55aa00", "#22dd22", "#55aa55",
             "#0000ff", "#5500aa", "#0055aa", "#2222dd", "#5555aa"]

transition, colormap = transition3, colormap3
'''

if COLORS == 2:
    transition, colormap = build2(K + 1)
elif COLORS == 3:
    transition, colormap = build3(K)
else:
    assert False

#print(transition, colormap)

tclrs = colormap[::len(transition) // COLORS]

data = [(len(transition) // COLORS) * from_distr(distrib) for _ in range(N * N)]

s = Simu(N, transition, data)

if NOTK:
    s.run()
    exit(0)

f = Tk()
f.configure(bg = 'white')
can = Canvas(f, width = N * PX, height = N * PX, bg = 'white')
can.grid(row = 0, column = 0, columnspan = COLORS, sticky = NSEW)
current_state = [-1] * (N * N)
objs = [None] * (N * N)
for i in range(N):
    for j in range(N):
        objs[i*N+j] = can.create_rectangle(i * PX, j * PX, (i + 1) * PX, (j + 1) * PX, fill = 'white', width = 0)
labels = [None] * COLORS
for i in range(COLORS):
    l = Label(f, text = "%d" % (s.count[i]), fg = tclrs[i], font = "Arial 50", bg = 'white')
    l.grid(row = 1, column = i, sticky = NS)

    l2 = Label(f, text = "%d" % (s.count[i]), fg = tclrs[i], font = "Arial 40", bg = 'white')
    l2.grid(row = 2, column = i, sticky = NS)
    
    labels[i] = l


s.start()

def loop():
    for i in range(len(s.count)):
        labels[i].configure(text = "%d" % s.count[i])
        #print("votes for color ", i, " : ", s.count[i])
    for i in range(len(s.data)):
        v = s.data[i]
        if v != current_state[i]:
            current_state[i] = v
            can.itemconfigure(objs[i], fill = colormap[v])
    f.after(5, loop)
loop()
f.mainloop()
